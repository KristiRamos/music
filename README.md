# Music  - Music and the Music of the Soul

Have you heard the application that compiled the music in one room? Indeed, you have it here! I have created a built-in platform that can apply in different genre of the song. I assure that you guys will like it. Track your favorite song and listen to it according to the genre, album, title and date created. 

Here you will hear the various genres and art by various artists. The accessibility is created through the following tools: [bournemouth man and van](http://manvanmove.co.uk/man-and-van-bournemouth/), Audio tool, SoundCloud, and YouTube. Feel free to discover the all in-one-music.

![Screenshot of music] (http://cdn.ghacks.net/wp-content/uploads/2010/09/download-free-music.png)